import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'model.dart';

class HttpService {
  final String baseUrl = "https://ruangsinggah.herokuapp.com";

  Future<List<Post>> getPosts() async {
    Response res = await get("$baseUrl/api/artikel");

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Post> posts = body
          .map(
            (dynamic item) => Post.fromJson(item),
          )
          .toList();
      return posts;
    } else {
      throw "Unable to retrieve posts.";
    }
  }

  Future<bool> createPost(Post data) async {
    final res = await post(
      "$baseUrl/api/artikel",
      headers: {"content-type": "application/json"},
      body: json.encode(data.toJson()),
    );
    if (res.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deletePost(int id) async {
    final response = await delete(
      "$baseUrl/api/artikel/$id",
      headers: {"content-type": "application/json"},
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updatePost(Post data) async {
    final res = await put(
      "$baseUrl/api/artikel/${data.id}",
      headers: {"content-type": "application/json"},
      body: json.encode(data.toJson()),
    );
    if (res.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
