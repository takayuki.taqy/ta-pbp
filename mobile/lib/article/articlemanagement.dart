import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'model.dart';
import 'api.dart';
import 'articledetail.dart';
import 'articleform.dart';
import 'articleedit.dart';

class PostManagementPage extends StatefulWidget {
  const PostManagementPage({Key? key}) : super(key: key);

  @override
  _PostManagementPageState createState() => _PostManagementPageState();
}

class _PostManagementPageState extends State<PostManagementPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Article Management Page"),
        backgroundColor: Color(0xff304ffe),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: PostManagement(),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color(0xff304ffe),
        label: Text("Add new Article"),
        icon: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FormAddScreen()),
          ).then((value) => setState(() {}));
        },
      ),
    );
  }
}

class PostManagement extends StatefulWidget {
  const PostManagement({Key? key}) : super(key: key);

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostManagement> {
  final HttpService httpService = HttpService();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: httpService.getPosts(),
      builder: (BuildContext context, AsyncSnapshot<List<Post>> snapshot) {
        if (snapshot.hasData) {
          List<Post>? posts = snapshot.data;
          return ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(
              parent: ClampingScrollPhysics(),
            ),
            children: posts!
                .map(
                  (Post post) => Card(
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                          leading: ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: 44,
                              minHeight: 44,
                              maxWidth: 64,
                              maxHeight: 64,
                            ),
                            child: Image.network(post.photo),
                          ),
                          title: Text(post.title),
                          subtitle:
                              Text(post.desc + " created by: " + post.author),
                          onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => PostDetail(
                                    post: post,
                                  ),
                                ),
                              )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          TextButton(
                            child: const Text('EDIT',
                                style: TextStyle(color: Colors.blue)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        EditFormScreen(post: post)),
                              ).then((value) => setState(() {}));
                            },
                          ),
                          const SizedBox(width: 8),
                          TextButton(
                            child: const Text('DELETE',
                                style: TextStyle(color: Colors.red)),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text("Warning"),
                                      content: Text(
                                          "Are you sure want to delete ${post.title} article?"),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text("Yes"),
                                          onPressed: () {
                                            Navigator.pop(context);
                                            httpService
                                                .deletePost(post.id)
                                                .then((isSuccess) {
                                              if (isSuccess) {
                                                setState(() {});
                                                Scaffold.of(this.context)
                                                    .showSnackBar(SnackBar(
                                                        content: Text(
                                                            "Delete data success")));
                                              } else {
                                                Scaffold.of(this.context)
                                                    .showSnackBar(SnackBar(
                                                        content: Text(
                                                            "Delete data failed")));
                                              }
                                            });
                                          },
                                        ),
                                        FlatButton(
                                          child: Text("No"),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        )
                                      ],
                                    );
                                  });
                            },
                          ),
                          const SizedBox(width: 8),
                        ],
                      )
                    ],
                  )),
                )
                .toList(),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
