import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'articleview.dart';
import 'articleform.dart';
import 'articlemanagement.dart';

class articlehome extends StatefulWidget {
  const articlehome({Key? key}) : super(key: key);

  @override
  _articlehomeState createState() => _articlehomeState();
}

class _articlehomeState extends State<articlehome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(
            parent: ClampingScrollPhysics(),
          ),
          children: [
            Container(
              margin: EdgeInsets.only(top: 30),
            ),
            _carousel(),
            Container(
              margin: EdgeInsets.all(30),
              child: ListView(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(
                  parent: ClampingScrollPhysics(),
                ),
                children: [
                  Text(
                    "Artikel terbaru untukmu!",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  PostPage()
                ],
              ),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Color(0xff304ffe),
          label: Text("Article Management"),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PostManagementPage()),
            ).then((value) => setState(() {}));
          },
        ));
  }

  Widget _carousel() {
    return CarouselSlider(
      items: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
                fit: BoxFit.cover, image: AssetImage("assets/carousel/1.png")),
          ),
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/carousel/2.png"))),
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/carousel/3.png"))),
        ),
      ],
      options: CarouselOptions(
        height: 200,
        aspectRatio: 16 / 9,
        viewportFraction: 0.8,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        enlargeCenterPage: true,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
