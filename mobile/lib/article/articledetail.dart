import 'package:flutter/material.dart';
import 'articleview.dart';
import 'model.dart';

class PostDetail extends StatelessWidget {
  final Post post;

  PostDetail({required this.post});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(post.title),
          backgroundColor: Color(0xff304ffe),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        title: Text(post.title),
                        subtitle: Text(post.desc),
                      ),
                      Container(
                        margin: EdgeInsets.all(15),
                        child: ListView(
                          physics: ClampingScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          children: [
                            ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxHeight: 300,
                                    minHeight: 200,
                                    maxWidth: 500,
                                    minWidth: 200),
                                child: Image.network(post.photo)),
                            Text(post.article)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
