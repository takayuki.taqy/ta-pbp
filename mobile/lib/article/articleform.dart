import 'package:flutter/material.dart';
import 'articleview.dart';
import 'api.dart';
import 'model.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class FormAddScreen extends StatefulWidget {
  @override
  _FormAddScreenState createState() => _FormAddScreenState();
}

class _FormAddScreenState extends State<FormAddScreen> {
  Future<Post>? _futureArticle;
  HttpService _httpService = HttpService();
  final TextEditingController _controllerTitle = TextEditingController();
  final TextEditingController _controllerAuthor = TextEditingController();
  final TextEditingController _controllerDesc = TextEditingController();
  final TextEditingController _controllerArticle = TextEditingController();
  final TextEditingController _controllerPhoto = TextEditingController();
  bool _isLoading = false;
  bool isTitleValid = false;
  bool isAuthorValid = false;
  bool isDescValid = false;
  bool isArticleValid = false;
  bool isPhotoValid = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldState,
        appBar: AppBar(
          title: Text("Article Submission Form"),
          centerTitle: true,
          backgroundColor: Color(0xff304ffe),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              TextField(
                  controller: _controllerTitle,
                  decoration: InputDecoration(
                      hintText: "Enter Article's Title",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                  onChanged: (value) {
                    isTitleValid = value.trim().isNotEmpty;
                  }),
              SizedBox(
                height: 15,
              ),
              TextField(
                  controller: _controllerAuthor,
                  decoration: InputDecoration(
                      hintText: "Enter Author's Name",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                  onChanged: (value) {
                    isAuthorValid = value.trim().isNotEmpty;
                  }),
              SizedBox(
                height: 15,
              ),
              TextField(
                controller: _controllerDesc,
                decoration: InputDecoration(
                    hintText: "Enter Article's Desc",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
                onChanged: (value) {
                  isDescValid = value.trim().isNotEmpty;
                },
              ),
              SizedBox(
                height: 15,
              ),
              TextField(
                  controller: _controllerArticle,
                  decoration: InputDecoration(
                      hintText: "Enter Your Article",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                  onChanged: (value) {
                    isArticleValid = value.trim().isNotEmpty;
                  }),
              SizedBox(
                height: 15,
              ),
              TextField(
                  controller: _controllerPhoto,
                  decoration: InputDecoration(
                      hintText: "Enter Image's Url",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                  onChanged: (value) {
                    isPhotoValid = value.trim().isNotEmpty;
                  }),
              SizedBox(
                height: 15,
              ),
              ElevatedButton(
                onPressed: () {
                  if (!isArticleValid ||
                      !isAuthorValid ||
                      !isDescValid ||
                      !isPhotoValid ||
                      !isTitleValid) {
                    _scaffoldState.currentState!.showSnackBar(
                      const SnackBar(
                        content: Text("Please fill all field"),
                      ),
                    );
                  } else {
                    setState(() => _isLoading = true);
                    String title = _controllerTitle.text.toString();
                    String author = _controllerAuthor.text.toString();
                    String desc = _controllerDesc.text.toString();
                    String article = _controllerArticle.text.toString();
                    String photo = _controllerPhoto.text.toString();
                    Post post = Post(
                        id: lastIndex,
                        title: title,
                        author: author,
                        date: DateTime.now().toString(),
                        desc: desc,
                        article: article,
                        photo: photo);
                    _httpService.createPost(post).then((isSuccess) {
                      setState(() => _isLoading = false);
                      if (isSuccess) {
                        Navigator.pop(context);
                      } else {
                        _scaffoldState.currentState!.showSnackBar(SnackBar(
                          content: Text("Submit data failed"),
                        ));
                      }
                    });
                  }
                },
                child: const Text("Create Article"),
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Color(0xff304ffe)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ))),
              )
            ],
          ),
        ));
  }
}
