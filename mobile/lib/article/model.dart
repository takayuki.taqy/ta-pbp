class Post {
  final int id;
  final String title;
  final String author;
  final String date;
  final String desc;
  final String article;
  final String photo;

  Post({
    required this.id,
    required this.title,
    required this.author,
    required this.date,
    required this.desc,
    required this.article,
    required this.photo,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json["id"],
      title: json['title'],
      author: json['author'],
      date: json['date'],
      desc: json['desc'],
      article: json['article'],
      photo: json['photo'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "title": title,
      "author": author,
      "date": date,
      "desc": desc,
      "article": article,
      "photo": photo,
    };
  }
}
