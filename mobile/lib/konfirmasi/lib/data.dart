import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'models/data_item.dart';

class Data {
  /*late List _data;

  Map fetched_data = {
    "data": [
      {
        "id": 1,
        "name": "abc",
        "npm": "2006531781",
        "date": "03/12/2001",
        "email": "test@gmail.com"
      },
      {
        "id": 2,
        "name": "pqr",
        "npm": "2006531781",
        "date": "03/12/2001",
        "email": "test@gmail.com"
      },
      {
        "id": 3,
        "name": "abc",
        "npm": "2006531781",
        "date": "03/12/2001",
        "email": "test@gmail.com"
      }
    ]
  };

  Data() {
    _data = fetched_data["data"];
  }

  int getId(int index) {
    return _data[index]["id"];
  }

  String getName(int index) {
    return _data[index]["name"];
  }

  String getNpm(int index) {
    return _data[index]["npm"];
  }

  String getJadwal(int index) {
    return _data[index]["date"];
  }

  String getEmail(int index) {
    return _data[index]["email"];
  }

  int getLength() {
    return _data.length;
  }*/

  static final List<DataItem> dataList = [
    DataItem(
      nama: 'Devina',
      npm: '2006531781',
      jadwal: '03/12/2022',
      email: 'text@gmail.com',
    ),
    DataItem(
      nama: 'Rafli',
      npm: '2006531781',
      jadwal: '04/12/2022',
      email: 'text@gmail.com',
    ),
  ];
}
