import 'package:flutter/material.dart';
import '../models/data_item.dart';
import '../main.dart';

class DataItemWidget extends StatelessWidget {
  final DataItem item;

  const DataItemWidget({
    required this.item,
  });

  @override
  Widget build(BuildContext context) => Align(
        child: Container(
            margin: const EdgeInsets.all(20.0),
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.blue[50],
              //shadowColor: Colors.grey.withOpacity(0.5),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                )
              ],
            ),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Column(children: [
                    Text(
                      'Konfirmasi Konsultasi\n',
                      textAlign: TextAlign.left,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    )
                  ])),
              Text(
                'Nama             : ${item.nama}\n',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
              Text(
                'NPM              : ${item.npm}\n ',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
              Text(
                'Jadwal          : ${item.jadwal}\n ',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
              Text(
                'Email             : ${item.email}\n ',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
              const SizedBox(height: 4),
              Center(
                child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: FlatButton(
                        padding: EdgeInsets.all(10.0),
                        child: Text('Accept',
                            style:
                                TextStyle(fontSize: 14, color: Colors.white)),
                        color: Colors.deepOrangeAccent,
                        //minWidth: 25.0,
                        //height: 15.0,
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                  "Selamat Anda telah menerima konsultasi",
                                  style: TextStyle(fontSize: 18)),
                              actions: <Widget>[
                                MaterialButton(
                                  elevation: 5.0,
                                  child: Text("close"),
                                  onPressed: () =>
                                      Navigator.pop(context), // passing false
                                ),
                              ],
                            ),
                          );
                        })),
              ),
              Center(
                child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: FlatButton(
                        padding: EdgeInsets.all(10.0),
                        child: Text('Reject',
                            style:
                                TextStyle(fontSize: 14, color: Colors.white)),
                        color: Colors.deepOrangeAccent,
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                    title: Text(
                                        "Sayang sekali, Anda telah menolak konsultasi",
                                        style: TextStyle(fontSize: 18)),
                                    actions: <Widget>[
                                      MaterialButton(
                                        elevation: 5.0,
                                        child: Text("close"),
                                        onPressed: () => Navigator.pop(
                                            context), // passing false          ],
                                      ),
                                    ],
                                  ));
                        })),
              ),
            ])),
      );
}
