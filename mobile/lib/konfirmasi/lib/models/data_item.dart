import 'package:meta/meta.dart';

class DataItem {
  final String nama;
  final String npm;
  final String jadwal;
  final String email;

  const DataItem({
    required this.nama,
    required this.npm,
    required this.jadwal,
    required this.email,
  });
}
