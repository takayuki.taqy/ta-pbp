import 'package:flutter/material.dart';
import '../data.dart';
import '../main.dart';

Widget MyText() => Align(
    child: Container(
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.all(10.0),
        padding: EdgeInsets.all(10.0),
        child: Column(children: [
          Text(
            'Konfirmasi Konsultasi',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          )
        ])));

void _showDialogOne(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Selamat Anda telah menerima konsultasi",
              style: TextStyle(fontSize: 20)),
          actions: <Widget>[
            FlatButton(
              child: Text("close"),
              onPressed: () => Navigator.pop(context), // passing false
            )
          ],
        );
      });
}

void _showDialogTwo(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            title: Text("Sayang sekali, Anda telah menolak konsultasi",
                style: TextStyle(fontSize: 20)),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text("close"),
                onPressed: () =>
                    Navigator.pop(context), // passing false          ],
              )
            ]);
      });
}
