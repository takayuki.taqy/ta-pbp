import 'package:flutter/material.dart';
import 'widget/data_widget.dart';
import 'data.dart';
import 'appbar.dart';
import 'navdrawer.dart';
import 'models/data_item.dart';

void main() {
  runApp(const MyKonfirmasi());
}

class MyKonfirmasi extends StatelessWidget {
  const MyKonfirmasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: appbar(),
        drawer: DrawerWidget(),
        body: MainPage(),
      ),
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

Widget MyText() => Align(
    child: Container(
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.all(10.0),
        padding: EdgeInsets.all(10.0),
        child: Column(children: [
          Text(
            'Konfirmasi Konsultasi',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          )
        ])));

class _MainPageState extends State<MainPage> {
  final items = List.from(Data.dataList);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
            child: AnimatedList(
              initialItemCount: items.length,
              itemBuilder: (context, index, animation) =>
                  buildItem(items[index], index),
            ),
          ),
        ]),
      );

  Widget buildItem(item, int index) => DataItemWidget(
        item: item,
        //animation: animation,
        //onClicked:() => removeItem(index),
      );

  void removeItem(int index) {
    final item = items.removeAt(index);

    /*currentState.removeItem(
      index,
      (context) => buildItem(item, index),
    );*/
  }
}
