import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'post_result_model.dart';

class HttpService {
  Future<bool> connectToAPI(PostResult data) async {
    String apiURL = "https://ruangsinggah.herokuapp.com/api/donasi";
    final apiResult = await post(
      apiURL,
      headers: {"content-type": "application/json"},
      body: json.encode(data.postJson()),
    );
    if (apiResult.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }
}
