import 'package:flutter/material.dart';
import 'post_result_model.dart';
import 'donasiapi.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class FormDonasi extends StatefulWidget {
  @override
  _FormDonasiState createState() => _FormDonasiState();
}

class _FormDonasiState extends State<FormDonasi> {
  HttpService _httpService = HttpService();
  String? selectedValue;
  final TextEditingController _controllerNama = TextEditingController();
  final TextEditingController _controllerBukti = TextEditingController();
  bool isNamaValid = false;
  bool isBuktiValid = false;
  bool _isLoading = false;

  final List<String> nominalItems = [
    'Rp50.000',
    'Rp100.000',
    'Rp250.000',
    'Rp500.000',
    'Rp1.000.000',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldState,
        appBar: AppBar(
          title: Text("Form Donasi"),
          centerTitle: true,
          backgroundColor: Color(0xff304ffe),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(3),
                child: Image.asset('assets/long.png', width: 400, height: 400),
              ),
              Padding(padding: EdgeInsets.only(top: 25.0)),
              TextField(
                  controller: _controllerNama,
                  decoration: InputDecoration(
                      hintText: "Masukkan nama kamu",
                      labelText: 'Nama',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                  onChanged: (value) {
                    isNamaValid = value.trim().isNotEmpty;
                  }),
              Padding(padding: EdgeInsets.only(top: 25.0)),
              DropdownButtonFormField(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(0, 5.5, 0, 0),
                    labelStyle: TextStyle(),
                    labelText: 'Nominal'),
                isExpanded: true,
                hint: const Text(
                  'Pilih nominal donasimu',
                  style: TextStyle(fontSize: 14),
                ),
                items: nominalItems
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ))
                    .toList(),
                validator: (value) {
                  if (value == null) {
                    return 'Please select nominal.';
                  }
                },
                onChanged: (value) {
                  //Do something when changing the item if you want.
                },
                onSaved: (value) {
                  selectedValue = value.toString();
                },
              ),
              Padding(padding: EdgeInsets.only(top: 25.0)),
              TextField(
                  controller: _controllerBukti,
                  decoration: InputDecoration(
                      hintText: "Masukkan url foto bukti transfer donasi",
                      labelText: 'Bukti Transfer',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                  onChanged: (value) {
                    isBuktiValid = value.trim().isNotEmpty;
                  }),
              Padding(padding: EdgeInsets.only(top: 25.0)),
              ElevatedButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                    backgroundColor: MaterialStateProperty.all<Color>(
                        const Color(0xff304ffe)),
                  ),
                  child: Text('Submit Donasi'),
                  onPressed: () {
                    if (!isNamaValid || !isBuktiValid) {
                      _scaffoldState.currentState!.showSnackBar(
                        const SnackBar(
                          content: Text("Please fill out all field"),
                        ),
                      );
                    } else {
                      setState(() => _isLoading = true);
                      String nama = _controllerNama.text.toString();
                      String nominal = selectedValue.toString();
                      String bukti_Transfer = _controllerBukti.text.toString();
                      PostResult postresult = PostResult(
                          nama: nama,
                          nominal: nominal,
                          bukti_Transfer: bukti_Transfer);
                      _httpService.connectToAPI(postresult).then((isSuccess) {
                        setState(() => _isLoading = false);
                        if (isSuccess) {
                          Navigator.pop(context);
                        } else {
                          _scaffoldState.currentState!.showSnackBar(SnackBar(
                            content: Text("Donasi gagal dimasukkan"),
                          ));
                        }
                      });
                    }
                  })
            ],
          ),
        ));
  }
}
