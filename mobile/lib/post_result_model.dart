import 'dart:convert';
import 'package:http/http.dart' as http;

class PostResult {
  String nama;
  String nominal;
  String bukti_Transfer;

  PostResult(
      {required this.nama,
      required this.nominal,
      required this.bukti_Transfer});

  factory PostResult.createPostResult(Map<String, dynamic> object) {
    return PostResult(
      nama: object['nama'],
      nominal: object['nominal'],
      bukti_Transfer: object['bukti_Transfer'],
    );
  }
  Map<String, dynamic> postJson() {
    return {
      "nama": nama,
      "nominal": nominal,
      "bukti_Transfer": bukti_Transfer,
    };
  }
}
