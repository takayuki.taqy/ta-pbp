import 'package:flutter/material.dart';
import 'donasi.dart';

class AddDonasi extends StatelessWidget {
  const AddDonasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 25.0)),
          Container(
              width: 500,
              padding: EdgeInsets.all(10),
              child: Text(
                  "Bantu RuangSinggah. untuk dapat terus singgah di hatimu",
                  style: TextStyle(
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center)),
          Container(
              width: 500,
              padding: EdgeInsets.all(10),
              child: Text(
                  "Dengan membantu RuangSinggah, kamu dapat membantu meningkatkan kesadaran tentang kesehatan mental di lingkungan kampus kita!",
                  style: TextStyle(fontSize: 15.0),
                  textAlign: TextAlign.center)),
          Container(
              width: 250,
              height: 250,
              padding: EdgeInsets.all(3),
              child: Image.asset('assets/jardonasi.png')),
          FloatingActionButton.extended(
            label: const Text('Donasi Sekarang'),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            foregroundColor: Colors.white,
            backgroundColor: const Color(0xff304ffe),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FormDonasi()),
              );
            },
          ),
        ]),
      ),
    );
  }
}
